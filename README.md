# Xceed Rest API extension Plugin

## Overview

This will add a new attribute in the JSON that is returned on the API call
`/posts` list. (More info: https://developer.wordpress.org/rest-api/reference/posts/)

It also adds a new endpoint for the API which gets all the posts ordered by view counts.
The namespace is: `post_views_counter_api/v1/` and the URL is `popular_posts`. The total path to this endpoint would
be like this:

```
http://yourWordPressSite/wp-json/post_views_counter_api/v1/popular_posts
```

It also activates the taxonomy language from the plugin polylang to be accessible via REST API
so you can query posts like this:

https://blog-xceedtest.dev/wp-json/wp/v2/posts?language=it

and they will be filtered by the language. 
 
## Requisites

1. This module requires the WordPress popular posts plugin to function 
(here: https://wordpress.org/plugins/wordpress-popular-posts/). (I am sure there is a way to explicit say that by code, but 
... sorry folks, next time. Just install this plugin first).

2. The API version of WordPress should be v2. I have no idea which WordPress version that means, but 
to be sure, I'd say it should be > 4.8.1

3. It also needs the multilingual plugin polylang: https://polylang.pro