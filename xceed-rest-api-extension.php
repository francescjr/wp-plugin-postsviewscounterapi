<?php
/*
Plugin Name: Xceed RestAPI Extension
Description: A brief description of the Plugin.
Version: 1.0
Author: cesc
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) )
	exit;

if ( ! class_exists( 'Post_Views_Counter_Api' ) ) :

	/**
	 * Post Views Counter final class.
	 *
	 * @class Post_Views_Counter_Api
	 * @version	0.1
	 */
	final class Xceed_Rest_Api_Extension
	{

	}

endif; // end if class_exists check


add_action( 'rest_api_init', 'xceed_rest_api_extension_add_language' );
add_action( 'rest_api_init', 'xceed_rest_api_extension_add_view_counter' );
add_action( 'rest_api_init', function () {
	register_rest_route( 'xceed_rest_api/v1', '/popular_posts', array(
		'methods' => 'GET',
		'callback' => 'xceed_rest_api_extension_get_popular_posts',
	) );
} );
add_action( 'rest_api_init', function () {
	register_rest_route( 'xceed_rest_api/v1', '/posts', array(
		'methods' => 'GET',
		'callback' => 'xceed_rest_api_extension_get_posts_by_language',
	) );
} );
function xceed_rest_api_extension_add_language()
{
	global $polylang;
	$default = pll_default_language();
	$langs = pll_languages_list();
	$cur_lang = $_GET['language'];
	if (!in_array($cur_lang, $langs)) {
		$cur_lang = $default;
	}
	$polylang->curlang = $polylang->model->get_language($cur_lang);
	$GLOBALS['text_direction'] = $polylang->curlang->is_rtl ? 'rtl' : 'ltr';

}
function xceed_rest_api_extension_add_view_counter()
{
	register_rest_field( 'post', 'view_counter', array(
		'get_callback' => function( $post_arr ) {
			return xceed_rest_api_extension_get_views_counter($post_arr['id']);
		},
		'update_callback' => function( $karma, $comment_obj ) {
			return true;
		},
		'schema' => array(
			'description' => __( 'Post View Counter.' ),
			'type'        => 'integer'
		),
	) );


	register_rest_field( 'post', 'language', array(
		'get_callback' => function( $post_arr ) {
			return xceed_rest_api_extension_get_language($post_arr['id']);
		},
		'update_callback' => function( $karma, $comment_obj ) {
			return true;
		},
		'schema' => array(
			'description' => __( 'Post View Counter.' ),
			'type'        => 'integer'
		),
	) );
}


function xceed_rest_api_extension_get_views_counter($id)
{
	global $wpdb;

	$fields = "p.pageviews AS 'pageviews'";
	$from = "{$wpdb->prefix}popularpostsdata p";
	$where = "p.postid = $id";

	// Build query
	$query = "SELECT {$fields} FROM {$from} WHERE {$where};";

	$result = $wpdb->get_results($query);

	$pageViews = 0;

	if (isset($result[0])) {
		$returnObject = $result[0];
		if (isset($returnObject->pageviews)) {
			$pageViews = (int) $returnObject->pageviews;
		}
	}

	return $pageViews;
}


function xceed_rest_api_extension_get_language($id)
{
	global $wpdb;

	$fields = "{$wpdb->terms}.slug as 'language'";
	$from = "{$wpdb->term_relationships} "
	        .
	        "left join {$wpdb->term_taxonomy} on {$wpdb->term_relationships}.term_taxonomy_id={$wpdb->term_taxonomy}.term_taxonomy_id ".
	        "left join {$wpdb->terms} on {$wpdb->term_taxonomy}.term_id={$wpdb->terms}.term_id";
	$where = "{$wpdb->term_relationships}.object_id = $id "
	         .
	         "and {$wpdb->term_taxonomy}.taxonomy='language'";

	// Build query
	$query = "SELECT {$fields} FROM {$from} WHERE {$where} ;";

	$result = $wpdb->get_results($query);

	$language = '';

	if (isset($result[0])) {
		$returnObject = $result[0];
		if (isset($returnObject->language)) {
			$language = $returnObject->language;
		}
	}

	return $language;
}

/**
 * Grab latest post title by an author!
 *
 * @param array $data Options for the function.
 * @return string|null Post title for the latest,  * or null if none.
 */
function xceed_rest_api_extension_get_popular_posts($data) {

	global $wpdb;

	$fields = "p.ID AS 'id', p.post_title AS 'title', p.post_date AS 'date', p.post_content AS 'content'";
	$fields .= ", v.pageviews AS 'pageviews'";
	$from = "{$wpdb->posts} p";
	$prefix = $wpdb->prefix . "popularposts";
	$from .= " LEFT JOIN {$prefix}data v ON p.ID = v.postid";
	$where = " p.post_type = 'post'";
	$orderby = "ORDER BY pageviews DESC";

	// List only published, non password-protected posts
	$where .= " AND p.post_password = '' AND p.post_status = 'publish'";

	// Build query
	$query = "SELECT {$fields} FROM {$from} WHERE {$where} {$orderby} LIMIT 10;";

	return $wpdb->get_results($query);

}

/**
 * Grab latest post title by an author!
 *
 * @param WP_REST_Request $request Options for the function.
 * @return string|null Post title for the latest,  * or null if none.
 */
function xceed_rest_api_extension_get_posts_by_language(WP_REST_Request $request) {

	global $wpdb;

	$lang = isset($request['lang']) ? $request['lang'] :  pll_default_language();

	$fields = "p.ID AS 'id', p.post_title AS 'title', p.post_date AS 'date', p.post_content AS 'content'";
	$fields .= ", v.pageviews AS 'pageviews'";
	$from = "{$wpdb->posts} p";
	$prefix = $wpdb->prefix . "popularposts";
	$from .= " LEFT JOIN {$prefix}data v ON p.ID = v.postid";
	$where = " p.post_type = 'post'";
	$orderby = "ORDER BY pageviews DESC";

	// List only published, non password-protected posts
	$where .= " AND p.post_password = '' AND p.post_status = 'publish'";

	// Build query
	$query = "SELECT {$fields} FROM {$from} WHERE {$where} {$orderby} LIMIT 10;";

	return $wpdb->get_results($query);

}